from tkinter import *      
from PIL import Image, ImageTk

root = Tk()      
root.title('Save Him Simulator')
canvas = Canvas(root, width = 300, height = 300, bg='white')      
canvas.pack()

verticalWall = Image.open("wallvertical.png")
verticalWall = ImageTk.PhotoImage(verticalWall)

horizontalWall = Image.open("wallhorizontal.png")
horizontalWall = ImageTk.PhotoImage(horizontalWall)

robot = Image.open("robot.png")
robot = ImageTk.PhotoImage(robot)

#img = PhotoImage(file = verticalWall)

canvas.create_image(20,20, anchor=NW, image = verticalWall)      
canvas.create_image(20,20, anchor=SW, image = horizontalWall)     
canvas.create_image(200,200, anchor=S, image = robot)      

mainloop()

"""
from tkinter import *
from PIL import ImageTk, Image
root = Tk()

canv = Canvas(root, width=80, height=80, bg='white')
canv.grid(row=2, column=3)

img = ImageTk.PhotoImage(Image.open("bll.jpg"))  # PIL solution
canv.create_image(20, 20, anchor=NW, image=img)

mainloop()
"""