from pyswip import Prolog
prolog = Prolog()

#prolog.consult('kb.pl')  # Mengimport fakta dari file yg udah ada, tp ga wajib  

# Mendeclare fakta kalo (X suka Y) dan (Y suka X) maka X dan Y adalah friends
prolog.assertz('friends(X,Y):-likes(X,Y),likes(Y,X)')  

prolog.assertz('likes(c,d)')    # Memasukkan fakta bahwa c menyukai d
prolog.assertz('likes(d,c)')    # Memasukkan fakta bahwa d menyukai c
prolog.assertz('likes(u,v)')    # Memasukkan fakta bahwa u menyukai v
prolog.assertz('likes(v,u)')    # Memasukkan fakta bahwa v menyukai u
prolog.assertz('likes(x,y)')    # Memasukkan fakta bahwa x menyukai y
#prolog.assertz('likes(y,x)')   <-- uncomment ini agar x dan y berteman

# Memprint siapa-siapa saja yang berteman
print(list(prolog.query('friends(PERSON1,PERSON2)'))) # <-- harus huruf kapital! 
'''
Output:
[{'PERSON2': 'd', 'PERSON1': 'c'}, 
{'PERSON2': 'c', 'PERSON1': 'd'}, 
{'PERSON2': 'v', 'PERSON1': 'u'}, 
{'PERSON2': 'u', 'PERSON1': 'v'}]
'''
"""
Stench(i, j) --> Wumpus(i, j+1) or Wumpus(i,j) or Wumpus(i+1, j) or Wumpus(i, j-1) or Wumpus(i-1, j)
Pit(i, j) --> Breeze(i, j+1) or Breeze(i+1, j) or Breeze(i, j-1) or Breeze(i-1, j)

"""