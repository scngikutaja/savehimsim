# SaveHimSim

## Team Members
- Dzaky Noor Hasyim - 1706021606
- Edrick Lainardi - 1706040113
- Gema Pratama Aditya - 1706040031
- William Gates - 1706040044

## Instructions

1. Clone this repository and type ```python main.py``` on the main directory.
2. Enter the desired height and width for the wumpus world. 
3. Tick auto if you want the agent to run, or untick auto if you want to manually run the world yourself.