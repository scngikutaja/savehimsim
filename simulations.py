from os import path
import sys
import time

import pygame

import agents
import constants
import environments


class Simulation:
    def __init__(self, args, world):
        self.args = args
        self.world = world
        self.window_size = [
            constants.GRID_WIDTH * self.args.width,
            constants.GRID_HEIGHT * self.args.height,
        ]
        self.setup_simulation()

    def draw(self):
        self.screen.fill(constants.WHITE)
        self.all_sprites.draw(self.screen)
        for y in range(self.args.height):
            for x in range(self.args.width):
                if not self.world.explored_areas[y][x]:
                    pygame.draw.rect(
                        self.screen,
                        constants.DARK_GREY,
                        [
                            constants.GRID_WIDTH * x,
                            constants.GRID_HEIGHT * y,
                            constants.GRID_WIDTH,
                            constants.GRID_HEIGHT,
                        ]
                    )
                if self.world.safe_areas[y][x] and self.args.show_safe:
                    pygame.draw.rect(
                        self.screen,
                        constants.BLUE,
                        [
                            constants.GRID_WIDTH * x,
                            constants.GRID_HEIGHT * y + constants.PERCEPT_HEIGHT * 3,
                            constants.PERCEPT_WIDTH,
                            constants.PERCEPT_HEIGHT,
                        ]
                    )
        self.draw_grid()
        pygame.display.flip()

    def draw_grid(self):
        for x in range(0, self.window_size[0], constants.GRID_WIDTH):
            pygame.draw.line(self.screen, constants.LIGHT_GREY, (x, 0), (x, self.window_size[1]))
        for y in range(0, self.window_size[0], constants.GRID_HEIGHT):
            pygame.draw.line(self.screen, constants.LIGHT_GREY, (0, y), (self.window_size[0], y))

    def handle_event(self, event):
        if event.type == pygame.QUIT:
            self.quit()
        if (self.args.auto) and (event.type == constants.GET_MOVEMENT):
            move = self.agent.get_movement()
            if move == constants.UP:
                self.agent.move(dy=-1)
            elif move == constants.RIGHT:
                self.agent.move(dx=1)
            elif move == constants.DOWN:
                self.agent.move(dy=1)
            elif move == constants.LEFT:
                self.agent.move(dx=-1)
        elif not self.args.auto:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.quit()
                if event.key == pygame.K_UP:
                    self.agent.move(dy=-1)
                if event.key == pygame.K_RIGHT:
                    self.agent.move(dx=1)
                if event.key == pygame.K_DOWN:
                    self.agent.move(dy=1)
                if event.key == pygame.K_LEFT:
                    self.agent.move(dx=-1)

    def run(self):
        self.setup_simulation()
        self.setup_sprites()
        self.run_simulation()

    def run_simulation(self):
        self.is_playing = True
        if self.args.auto:
            pygame.time.set_timer(constants.GET_MOVEMENT, 1000)
        while self.is_playing:
            self.dt = self.clock.tick(constants.FPS) / 1000
            for event in pygame.event.get():
                self.handle_event(event)
            self.update()
            self.draw()
            if not self.agent.is_alive:
                print('Agent failed to solve the given world.')
                time.sleep(3)
                self.is_playing = False
            elif environments.Robot.total_robots == 0:
                print('Agent managed to save all the robots.')
                time.sleep(3)
                self.is_playing = False
        time.sleep(0.8)

    def setup_simulation(self):
        pygame.init()
        pygame.display.set_caption(constants.TITLE)
        pygame.display.set_icon(pygame.image.load('icon.png'))
        pygame.key.set_repeat(500, 100)
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode(self.window_size)

    def setup_sprites(self):
        self.all_sprites = pygame.sprite.Group()
        self.agent = agents.Agent(self, 0, 0)
        self.environments = pygame.sprite.Group()
        self.percepts = pygame.sprite.Group()
        self.world.explored_areas[0][0] = True
        self.world.safe_areas[0][0] = True
        self.world.initialize_environments(self)
        self.world.initialize_percepts(self)

    def update(self):
        self.all_sprites.update()

    def quit(self):
        pygame.quit()
        sys.exit()
