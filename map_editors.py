import argparse
import os
import sys

import pygame

import agents
import constants
import environments
import percepts
import worlds


class MapEditor:
    ENVIRONMENT_CLASSES = {
        'hole': environments.Hole,
        'lava': environments.Lava,
        'robot': environments.Robot,
    }

    def __init__(self, args):
        self.args = args
        self.selected_environment = next(iter(self.ENVIRONMENT_CLASSES.values()))
        self.window_size = [
            constants.GRID_WIDTH * self.args.width + constants.GRID_MARGIN * (self.args.width + 1),
            constants.GRID_HEIGHT * self.args.height + constants.GRID_MARGIN * (self.args.height + 1),
        ]

    def draw_environments(self):
        for y in range(self.args.height):
            for x in range(self.args.width):
                environment = self.world.get_environment(x, y)
                if environment:
                    pygame.draw.rect(
                        self.screen,
                        environment.COLOR,
                        [
                            (constants.GRID_MARGIN + constants.GRID_WIDTH) * x + constants.GRID_MARGIN,
                            (constants.GRID_MARGIN + constants.GRID_HEIGHT) * y + constants.GRID_MARGIN,
                            constants.GRID_WIDTH,
                            constants.GRID_HEIGHT,
                        ]
                    )

    def draw_world(self):
        self.screen.fill(constants.BLACK)
        for y in range(self.args.height):
            for x in range(self.args.width):
                pygame.draw.rect(
                    self.screen,
                    constants.WHITE,
                    [
                        (constants.GRID_MARGIN + constants.GRID_WIDTH) * x + constants.GRID_MARGIN,
                        (constants.GRID_MARGIN + constants.GRID_HEIGHT) * y + constants.GRID_MARGIN,
                        constants.GRID_WIDTH,
                        constants.GRID_HEIGHT,
                    ]
                )
        self.draw_environments()
        self.clock.tick(constants.FPS)
        pygame.display.flip()

    def handle_event(self, event):
        if event.type == pygame.QUIT:
            self.quit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            pos = pygame.mouse.get_pos()
            x = pos[0] // (constants.GRID_WIDTH + constants.GRID_MARGIN)
            y = pos[1] // (constants.GRID_HEIGHT + constants.GRID_MARGIN)
            self.world.update_environment(self.selected_environment, x, y)                
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_h:
                self.selected_environment = self.ENVIRONMENT_CLASSES['hole']
            elif event.key == pygame.K_l:
                self.selected_environment = self.ENVIRONMENT_CLASSES['lava']
            elif event.key == pygame.K_r:
                self.selected_environment = self.ENVIRONMENT_CLASSES['robot']
            elif event.key == pygame.K_RETURN:
                self.world_setup_completed = True
            elif event.key == pygame.K_BACKSPACE:
                self.selected_environment = None
        

    def run(self):
        self.setup_map_editor()
        return self.run_map_editor()

    def run_map_editor(self):
        while not self.world_setup_completed:
            for event in pygame.event.get():
                self.handle_event(event)
            self.draw_world()
        pygame.quit()
        return self.world

    def setup_map_editor(self):
        try:
            pygame.init()
            pygame.display.set_caption(constants.TITLE)
            pygame.display.set_icon(pygame.image.load('icon.png'))
            self.clock = pygame.time.Clock()
            self.screen = pygame.display.set_mode(self.window_size)
            self.world = worlds.World(self.args.width, self.args.height)
        except pygame.error:
            print("\nPlease enter positive integer greater than 2 for world size.")
            sys.exit(0)
        self.world_setup_completed = False

    def quit(self):
        pygame.quit()
        sys.exit()
