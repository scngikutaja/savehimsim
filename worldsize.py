from tkinter import ttk
import tkinter as tk

arguments = dict()

class Gui():
    def __init__(self, top):
        self.top = top
        self.top.geometry("+10+10")

        self.frame = tk.Frame(self.top)
        self.frame.grid()

        labelWidth = tk.Label(self.frame, text="Width").grid(row=0)
        lebelHeight = tk.Label(self.frame, text="Height").grid(row=1)
        lebelAuto = tk.Label(self.frame, text="Auto").grid(row=2)
        lebelSafe = tk.Label(self.frame, text="Show Safe Area(s)").grid(row=3)

        entryWidth = tk.Entry(self.frame)
        entryHeight = tk.Entry(self.frame)
        entryAuto = tk.IntVar(self.frame)
        checkButtonAuto = tk.Checkbutton(self.frame, variable=entryAuto)
        entrySafe = tk.IntVar(self.frame)
        checkButtonSafe = tk.Checkbutton(self.frame, variable=entrySafe)

        entryWidth.grid(row=0, column=1)
        entryHeight.grid(row=1, column=1)
        checkButtonAuto.grid(row=2, column=1)
        checkButtonSafe.grid(row=3, column=1)

        ttk.Button(self.frame, text='Exit', command=self.frame.quit).grid(row=4, column=0, sticky=tk.W, pady=4)
        ttk.Button(self.frame, text='Enter', command= lambda: self.check_input(entryWidth, entryHeight, entryAuto, entrySafe)).grid(row=4, column=1, sticky=tk.W, pady=4)

    def check_input(self, width, height, auto, safe):
        try:
            self.save_value_show_hint(int(width.get()), int(height.get()), auto.get(), safe.get())
        except ValueError:
            print("Error entry is not int")

    def save_value_show_hint(self, width, height, auto, safe):
        arguments['width'] = width
        arguments['height'] = height
        arguments['auto'] = auto
        arguments['safe'] = safe
        self.frame.destroy()
        T = tk.Label(root, text="Click on a tile to set environment\nThese keys will change the environment placed\nH = Hole (Brown)\nL = Lava (Red)\nR = Robot (Green)\nBackspace = Clear the selected cell").grid(row=0, column=0)
        ttk.Button(root, text='Proceed', command=root.quit).grid(row=6, column=0)

root = tk.Tk()
root.title('SaveHimSim')
gui = Gui(root)
root.mainloop()
