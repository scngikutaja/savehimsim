:- dynamic
    assume_hole/1,
    assume_lava/1,
    no_hole/1,
    no_lava/1.

perceive_heat(no, [X, Y]) :- 
    A1 is X+1, retractall(assume_lava([A1, Y])), assert(no_lava([A1, Y])),
    A2 is X-1, retractall(assume_lava([A2, Y])), assert(no_lava([A2, Y])),
    A3 is Y-1, retractall(assume_lava([X, A3])), assert(no_lava([X, A3])),
    A4 is Y+1, retractall(assume_lava([X, A4])), assert(no_lava([X, A4])).

perceive_heat(yes, [X, Y]) :- 
    A1 is X+1, (no_lava([A1, Y]); assert(assume_lava([A1, Y]))),
    A2 is X-1, (no_lava([A2, Y]); assert(assume_lava([A2, Y]))),
    A3 is Y-1, (no_lava([X, A3]); assert(assume_lava([X, A3]))),
    A4 is Y+1, (no_lava([X, A4]); assert(assume_lava([X, A4]))).

perceive_slope(no, [X, Y]) :- 
    A1 is X+1, retractall(assume_hole([A1, Y])), assert(no_hole([A1, Y])),
    A2 is X-1, retractall(assume_hole([A2, Y])), assert(no_hole([A2, Y])),
    A3 is Y-1, retractall(assume_hole([X, A3])), assert(no_hole([X, A3])),
    A4 is Y+1, retractall(assume_hole([X, A4])), assert(no_hole([X, A4])). 

perceive_slope(yes, [X, Y]) :-
    A1 is X+1, (no_hole([A1, Y]); assert(assume_hole([A1, Y]))), 
    A2 is X-1, (no_hole([A2, Y]); assert(assume_hole([A2, Y]))),
    A3 is Y-1, (no_hole([X, A3]); assert(assume_hole([X, A3]))),
    A4 is Y+1, (no_hole([X, A4]); assert(assume_hole([X, A4]))).

run :-
    perceive_heat(no, [0, 0]),
    perceive_slope(no, [0, 0]).
