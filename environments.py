import pygame

import constants
import percepts


class Environment(pygame.sprite.Sprite):
    def __init__(self, simulation, x, y):
        self.groups = (simulation.all_sprites, simulation.environments)
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.simulation = simulation
        self.image = pygame.Surface((constants.GRID_WIDTH, constants.GRID_HEIGHT))
        self.image.fill(self.COLOR)
        self.percepts = []
        self.rect = self.image.get_rect()
        self.rect.x = x * constants.GRID_WIDTH
        self.rect.y = y * constants.GRID_HEIGHT
        self.x = x
        self.y = y

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class Hole(Environment):
    COLOR = constants.BROWN
    IS_TOUCHABLE = False
    NAME = 'Hole'
    PERCEPT_CLASS = percepts.Slope

    def __init__(self, simulation, x, y):
        self.percepts = []
        super().__init__(simulation, x, y)


class Lava(Environment):
    COLOR = constants.RED
    IS_TOUCHABLE = False
    NAME = 'Lava'
    PERCEPT_CLASS = percepts.Heat

    def __init__(self, simulation, x, y):
        self.percepts = []
        super().__init__(simulation, x, y)


class Robot(Environment):
    COLOR = constants.GREEN
    IS_TOUCHABLE = True
    NAME = 'Robot'
    PERCEPT_CLASS = None
    total_robots = 0

    def __init__(self, simulation, x, y):
        Robot.total_robots += 1
        super().__init__(simulation, x, y)

    def touch(self):
        self.kill()
        self.simulation.world.delete_environment(self)
        Robot.total_robots -= 1
        print('Touched {}.'.format(self.NAME))
