import pygame

import constants


class Percept(pygame.sprite.Sprite):
    def __init__(self, simulation, x, y):
        self.groups = (simulation.all_sprites, simulation.percepts)
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.simulation = simulation
        self.image = pygame.Surface((constants.PERCEPT_WIDTH, constants.PERCEPT_HEIGHT))
        self.image.fill(self.COLOR)
        self.rect = self.image.get_rect()
        self.rect.x = x * constants.GRID_WIDTH
        self.rect.y = y * constants.GRID_HEIGHT + self.SPRITE_Y_OFFSET
        self.x = x
        self.y = y


class Heat(Percept):
    COLOR = constants.LIGHT_RED
    NAME = 'Heat'
    SPRITE_Y_OFFSET = constants.PERCEPT_HEIGHT * 0


class Slope(Percept):
    COLOR = constants.LIGHT_BROWN
    NAME = 'Slope'
    SPRITE_Y_OFFSET = constants.PERCEPT_HEIGHT * 1
