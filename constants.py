import pygame

# Color constants

BLACK = (0, 0, 0)

BLUE = (0, 0, 255)

BROWN = (139, 69, 19)

DARK_GREY = (40, 40, 40)

GREEN = (0, 255, 0)

LIGHT_BROWN = (139, 99, 49)

LIGHT_GREEN = (120, 255, 120)

LIGHT_GREY = (100, 100, 100)

LIGHT_RED = (255, 120, 120)

RED = (255, 0, 0)

YELLOW = (255, 255, 0)

WHITE = (255, 255, 255)

# Grid size constants

GRID_HEIGHT = 32

GRID_MARGIN = 3

GRID_WIDTH = 32

PERCEPT_HEIGHT = GRID_HEIGHT * 0.2

PERCEPT_WIDTH = GRID_WIDTH

# Pygame constants

FPS = 60

GET_MOVEMENT = pygame.USEREVENT + 1

TITLE = 'SaveHimSim'

UP = 'up'

RIGHT = 'right'

DOWN = 'down'

LEFT = 'left'
