class World:
    def __init__(self, width, height):
        self.environments = []
        self.explored_areas = []
        self.height = height
        self.percepts = []
        self.safe_areas = []
        self.width = width
        for y in range(self.height):
            self.environments.append([])
            self.explored_areas.append([])
            self.percepts.append([])
            self.safe_areas.append([])
            for x in range(self.width):
                self.environments[y].append(None)
                self.explored_areas[y].append(False)
                self.percepts[y].append([])
                self.safe_areas[y].append(False)

    def delete_environment(self, environment):
        self.environments[environment.y][environment.x] = None

    def delete_percept(self, percept):
        self.percepts[percept.y][percept.x].remove(percept)

    def get_environment(self, x, y):
        return self.environments[y][x]

    def get_percepts(self, x, y):
        return self.percepts[y][x]

    def initialize_environments(self, simulation):
        for y in range(self.height):
            for x in range(self.width):
                environment = self.environments[y][x]
                if environment:
                    self.environments[y][x] = environment(simulation, x, y)

    def initialize_percepts(self, simulation):
        for y in range(self.height):
            for x in range(self.width):
                environment = self.environments[y][x]
                if environment and environment.PERCEPT_CLASS:
                    for row_neighbour in (-1, 1):
                        if (x + row_neighbour < self.width) and (x + row_neighbour >= 0) and (y < self.height) and (y >= 0):
                            percept = environment.PERCEPT_CLASS(simulation, x + row_neighbour, y)
                            self.environments[y][x].percepts.append(percept)
                            self.percepts[y][x + row_neighbour].append(percept)
                    for column_neighbour in (-1, 1):
                        if (x < self.width) and (x >= 0) and (y + column_neighbour < self.height) and (y + column_neighbour >= 0):
                            percept = environment.PERCEPT_CLASS(simulation, x, y + column_neighbour)
                            self.environments[y][x].percepts.append(percept)
                            self.percepts[y + column_neighbour][x].append(percept)

    def update_environment(self, environment, x, y):
        if ((x == 0) and (y == 0)) or ((x == 0) and (y == 1)) or ((x == 1) and (y == 0)):
            print('Environment can\'t be placed at ({}, {}).'.format(x, y))
        else:
            self.environments[y][x] = environment
            if environment == None:
                print('({}, {}) cleared.'.format(x, y))
            else:    
                print('{} placed at ({}, {}).'.format(environment.NAME, x, y))
