import random
import time

import pygame
import pyswip

import constants
import environments
import percepts

class Agent(pygame.sprite.Sprite):
    COLOR = constants.YELLOW
    ENVIRONMENT_CLASSES = {
        'hole': environments.Hole,
        'lava': environments.Lava,
        'robot': environments.Robot,
    }
    NAME = 'Agent'
    PERCEPT_CLASSES = {
        'heat': percepts.Heat,
        'slope': percepts.Slope,
    }
    possible_moves = []

    def __init__(self, simulation, x, y):
        self.groups = simulation.all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.image = pygame.Surface((constants.GRID_WIDTH, constants.GRID_HEIGHT))
        self.image.fill(self.COLOR)
        self.is_alive = True
        self.prolog = pyswip.Prolog()
        self.prolog.consult('kb.pl')
        list(self.prolog.query('run'))
        self.rect = self.image.get_rect()
        self.simulation = simulation
        self.update_safe_areas()
        self.x = x
        self.y = y

    def collide_with_walls(self, dx=0, dy=0):
        return ((self.x + dx < 0) or (self.y + dy < 0) or (self.x + dx >= self.simulation.world.width) or (self.y + dy >= self.simulation.world.height))

    def filter_query_results(self, query_results):
        filtered_query_results = []
        for query_result in query_results:
            for key, value in query_result.items():
                if (value[0] < self.simulation.world.width) and (value[0] >= 0) and (value[1] < self.simulation.world.height) and (value[1] >= 0):
                    filtered_query_results.append((value[0], value[1]))
        return filtered_query_results

    def get_collided_environment(self):
        return self.simulation.world.get_environment(self.x, self.y)

    def get_collided_percepts(self):
        return self.simulation.world.get_percepts(self.x, self.y)

    def get_movement(self):
        self.update_moves()
        if self.possible_moves is not None:
            if self.possible_moves[0][0] < self.y:
                return constants.UP
            elif self.possible_moves[0][1] > self.x:
                return constants.RIGHT
            elif self.possible_moves[0][1] < self.x:
                return constants.LEFT
            elif self.possible_moves[0][0] > self.y:
                return constants.DOWN
            elif self.possible_moves[0][0] == self.y and self.possible_moves[0][1] == self.x:
                self.possible_moves.pop(0)
                if len(self.possible_moves) == 0:
                    return "LOSE"

    def random_move(self, possible_moves):
        return random.choice(possible_moves)

    def update_moves(self):
        for i in range(self.simulation.world.width):
            for j in range(self.simulation.world.height):
                if (self.simulation.world.explored_areas[j][i] == False) and (self.simulation.world.safe_areas[j][i] == True):
                    if ([i,j]) not in self.possible_moves:
                        self.possible_moves.append([i,j])

    def move(self, dx=0, dy=0):
        if not self.collide_with_walls(dx, dy):
            self.x += dx
            self.y += dy
        environment = self.get_collided_environment()
        if environment:
            if environment.IS_TOUCHABLE:
                environment.touch()
            else:
                self.is_alive = False
        self.start_inference()

    def update(self):
        self.rect.x = self.x * constants.GRID_WIDTH
        self.rect.y = self.y * constants.GRID_HEIGHT

    def update_safe_areas(self):
        safe_areas = []
        for name, environment_class in self.ENVIRONMENT_CLASSES.items():
            if not environment_class.IS_TOUCHABLE:
                safe_areas.append(set(self.filter_query_results(list(self.prolog.query('no_{}(X)'.format(name))))))
        safe_areas = set.intersection(*safe_areas)
        for safe_area in safe_areas:
            self.simulation.world.safe_areas[safe_area[1]][safe_area[0]] = True

    def start_inference(self):
        if not self.simulation.world.explored_areas[self.y][self.x]:
            perceived_perceptions = []
            percepts = self.get_collided_percepts()
            for percept in percepts:
                if percept.NAME not in perceived_perceptions:
                    for key, value in self.PERCEPT_CLASSES.items():
                        if percept.NAME == value.NAME:
                            list(self.prolog.query('perceive_{}(yes, [{}, {}])'.format(key, self.x, self.y)))
                            print('Perceived {} at ({}, {}).'.format(percept.NAME, self.x, self.y))
                            perceived_perceptions.append(percept.NAME)
            for name, percept_class in self.PERCEPT_CLASSES.items():
                if percept_class.NAME not in perceived_perceptions:
                    list(self.prolog.query('perceive_{}(no, [{}, {}])'.format(name, self.x, self.y)))
                    print('Didn\'t perceive {} at ({}, {}).'.format(percept_class.NAME, self.x, self.y))
            self.simulation.world.explored_areas[self.y][self.x] = True
            self.update_safe_areas()
