import argparse

import map_editors
import simulations

import worldsize

class Namespace:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

def parse_command_line_args():
    parser = argparse.ArgumentParser(description='Start SaveHimSim simulation.')
    parser.add_argument('-a', '--auto', action='store_true', default=False, help='Enable auto simulation.')
    parser.add_argument('-x', '--width', help='Set desired map width.', required=True, type=int)
    parser.add_argument('-y', '--height', help='Set desired map height.', required=True, type=int)
    return parser.parse_args()

def main():
    gui = worldsize
    args = Namespace(auto=gui.arguments['auto'], width=gui.arguments['width'], height=gui.arguments['height'], show_safe=gui.arguments['safe'])
    map_editor = map_editors.MapEditor(args)
    world = map_editor.run()
    simulation = simulations.Simulation(args, world)
    simulation.run()
    
    
if __name__ == '__main__':
    main()
